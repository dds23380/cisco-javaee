/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cisco.jee.ws.firstclientws;

import java.util.List;
import java.util.Scanner;

/**
 *
 * @author david
 */
public class Ejecutador {
    public static void main(String[] args) {
        System.out.println("Message to send: ");
        String msg0="";
        Scanner s = new Scanner(System.in);
        msg0 = s.nextLine();
        while(!msg0.isEmpty()){
            setMSG(msg0);
            msg0 = s.nextLine();
        }
        
        
        
        System.out.println("Messages list...");
        for(String msg:getMSGs()){
            System.out.println("Message: "+msg);
        }
    }
    
    private static List<String> getMSGs(){
        org.cisco.jee.ws.firstclientws.Messages_Service service = new org.cisco.jee.ws.firstclientws.Messages_Service();
        org.cisco.jee.ws.firstclientws.Messages port = service.getMessagesPort();
        return port.getMessages();
    }
    
    private static String setMSG(String msg){
        org.cisco.jee.ws.firstclientws.Messages_Service service = new org.cisco.jee.ws.firstclientws.Messages_Service();
        org.cisco.jee.ws.firstclientws.Messages port = service.getMessagesPort();
        return port.setMessage(msg);
    }
}
