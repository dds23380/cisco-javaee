/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cisco.jee.ws.firstserviceweb;

import java.util.LinkedList;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author david
 */
@WebService(serviceName = "Messages")
public class Messages {

    private static List<String> messages = new LinkedList<>();
    
    /**
     * This is a set message service operation
     * @param txt
     * @return 
     */
    @WebMethod(operationName = "setMessage")
    public String setMessage(@WebParam(name = "msg") String txt) {
        messages.add(txt);
        return "OK";
    }
    
    /**
     * This is a get message service operation
     * @return 
     */
    @WebMethod(operationName = "getMessages")
    public List<String> getMessages() {
        return messages;
    }
    
}
