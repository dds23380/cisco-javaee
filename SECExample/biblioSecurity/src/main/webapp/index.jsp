<%-- 
    Document   : index
    Created on : 4 dic. 2020, 16:34:21
    Author     : david
--%>

<%@page import="org.cisco.jee.security.bibliosecurity.Book"%>
<%@page import="org.cisco.jee.security.bibliosecurity.BiblioServiceLocal"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="org.cisco.jee.security.bibliosecurity.servlets.index"%>
<%@page session="true" language="java" contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Pagina inicio BiblioService</title>
    </head>
    <body>
        
         <%
             BiblioServiceLocal service = ((index)getServletContext().getAttribute("index")).getService();
             String user  = service.getUserLogged();
             String group = service.getGroupLogged();
             pageContext.setAttribute("user", user);
             pageContext.setAttribute("group", group);
         %>
        
        <h1>Usuario activo es <%=user%> </h1>
            
        <h2>Consola general:</h2>
        
        <form method="post">
            <label for="title"><b>Busqueda por titulo:</b></label>
            <input id="title" type="text" name="title" required>
            <input type="submit" value="Buscar">
        </form>
        <%
            String title = request.getParameter("title");
            if(title!=null&&!title.isEmpty()){
                Book book = service.getByTitle(title);
                if(book!=null){
                    %>
                    <p><b style="color:red" font-size:16px>Titulo: </b><b style="color:blue"><%=book.getTitle()%></b></p>
                    <p><b style="color:red" font-size:16px>Descripcion: </b><b style="color:blue"><%=book.getDescription()%></b></p>
                    <p><b style="color:red" font-size:16px>Tipo: </b><b style="color:blue"><%=book.getType()%></b></p>
                    <br/>
                    <%
                } else {
                    %>
                        <h3 style="color:red">Libro no encontrado</h3>
                    <%
                }
            }
        %>
        <br/><br/>
        <c:if test="${group eq 'user'}">
            <jsp:include  page="/board_user" flush="true"/>
        </c:if>
        
        <c:if test="${group eq 'admin'}">
            <jsp:include  page="/board_user" flush="true"/>
            <jsp:include  page="/board_admin" flush="true"/>
        </c:if>
        
        <h2>Panel de acciones:</h2>
        <p>Sesion: 
        <c:choose>
            <c:when test="${user eq 'ANONYMOUS'}">
                <button type="button" onclick="window.location.href='<%=request.getContextPath()%>/login'">login</button>
            </c:when>
            <c:otherwise>
                <button type="button" onclick="window.location.href='<%=request.getContextPath()%>/logout'">logout</button>
            </c:otherwise>
        </c:choose>
        </p>
    </body>
</html>
