<%-- 
    Document   : logout
    Created on : 4 dic. 2020, 15:06:27
    Author     : david
--%>

<%@page session="true" contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Pagina de Logout</title>
    </head>
    <body>
        Usuario '<%=request.getRemoteUser()%>' ah cerrado su sesion.
        <% session.invalidate(); %>
        <br/><br/>
        <button onclick="window.location.href=<%=request.getContextPath()%>/">Volver</button>
    </body>
</html>