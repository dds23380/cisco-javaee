/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cisco.jee.security.bibliosecurity;

import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author david
 */
@Local
public interface BiblioServiceLocal {

    void addBook(String title, String description, String type, long id);

    void removeBookById(long id);

    List<Book> getAll();

    Book getByTitle(String title);

    void removeByTitle(String title);

    String getUserLogged();

    boolean isAdmin();

    String getGroupLogged();

    boolean isUser();
    
    
}
