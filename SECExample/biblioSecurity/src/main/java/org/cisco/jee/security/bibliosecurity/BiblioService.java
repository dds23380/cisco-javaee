/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cisco.jee.security.bibliosecurity;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.annotation.security.DeclareRoles;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;

/**
 *
 * @author david
 */
@DeclareRoles(value={"admin","user"})
@Stateful
public class BiblioService implements BiblioServiceLocal {
    @Resource SessionContext ctx;
    
    private List<Book> lists = new ArrayList<>();
    
    @RolesAllowed(value={"admin"})
    @Override
    public void addBook(String title, String description, String type, long id) {
        if(title==null && title.isEmpty()){
            return;
        }
        if(id==0){
            id=(long) (Math.random() * 1000);
        }
        String name = ctx.getCallerPrincipal().getName();
        System.out.println("Administrador "+name);
        System.out.println("Creando nuevo libro "+title);
        Book book = new Book();
        book.setTitle(title);
        book.setDescription(description);
        book.setType(type);
        book.setId(id);
        lists.add(book);
    }
    
    

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")

    @RolesAllowed(value={"admin"})
    @Override
    public void removeBookById(long id) {
        String name = ctx.getCallerPrincipal().getName();
        System.out.println("Administrador "+name);
        System.out.println("Eliminando libro por id = "+id);
        for(Book book: lists){
            if(book.getId()==id){
                System.out.println("Eliminado libro "+book.getTitle());
                lists.remove(book);
                return;
            }
        }
        System.out.println("Libro por ID no se encontro");
    }
    
    @RolesAllowed(value={"admin","user"})
    @Override
    public List<Book> getAll() {
        String name = ctx.getCallerPrincipal().getName();
        System.out.println("El usuario "+name);
        if(ctx.isCallerInRole("admin")){
            System.out.println("Usuario con role admin realiza un getAll");
        } else if (ctx.isCallerInRole("user")){
            System.out.println("Usuario con role user realiza un getAll");
        }
        return lists;
    }

    @PermitAll
    @Override
    public Book getByTitle(String title) {
        String name = ctx.getCallerPrincipal().getName();
        if(ctx.isCallerInRole("admin")){
            System.out.println("Usuario con role admin realiza un get por titulo");
        } else if (ctx.isCallerInRole("user")){
            System.out.println("Usuario con role user realiza un get por titulo");
        } else {
            System.out.println("Usuario sin autenticar realiza un get por titulo");
        }
        for(Book book: lists){
            if(book.getTitle().equals(title)){
                System.out.println("Libro encontrado "+book.getTitle());
                return book;
            }
        }
        System.out.println("Libro no encontrado");
        return null;
    }

    @RolesAllowed(value={"admin"})
    @Override
    public void removeByTitle(String title) {
        System.out.println("Administrador "+title);
        System.out.println("Eliminando libro por titulo");
        for(Book book: lists){
            if(book.getTitle().equals(title)){
                System.out.println("Eliminado "+book.getTitle());
                lists.remove(book);
                return;
            }
        }
        System.out.println("libro no eliminado ");
    }

    @PermitAll
    @Override
    public String getUserLogged() {
        return ctx.getCallerPrincipal().getName();
    }

    @PermitAll
    @Override
    public boolean isAdmin() {
        return ctx.isCallerInRole("admin");
    }

    @PermitAll
    @Override
    public String getGroupLogged() {
        if(isAdmin()){
            return "admin";
        }
        if(isUser()){
            return "user";
        }
        return "NONE";
    }

    @Override
    public boolean isUser() {
        return ctx.isCallerInRole("user");
    }
    
    
    
    
    
}
