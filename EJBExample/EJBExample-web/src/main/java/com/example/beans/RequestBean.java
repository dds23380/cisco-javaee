/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.beans;

import com.example.TechnicalBeanLocal;
import com.example.model.TechRequest;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author MARTIN
 */
@ManagedBean
@SessionScoped
public class RequestBean implements Serializable {

    //request data:
    private String email, software, so, problem;

    //customer data:
    @EJB
    private TechnicalBeanLocal technicalBean;

    public RequestBean() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSoftware() {
        return software;
    }

    public void setSoftware(String software) {
        this.software = software;
    }

    public String getSo() {
        return so;
    }

    public void setSo(String so) {
        this.so = so;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public String[] getSoftwareList() {
        return new String[]{"Microsoft Word", "Microsoft Excel", "Microsoft Access"};
    }

    public String[] getSOList() {
        return new String[]{"Windows 7", "Windows 10", "Windows Vista"};
    }
    
    public void reset(){
        this.email="";
        this.problem = "";
    }
    public String registerRequest() {
        TechRequest req = new TechRequest();
        req.setEmail(email);
        req.setSo(so);
        req.setSoftware(software);
        req.setDetail(problem);
        problem = email = null;

        technicalBean.registerRequest(req);
        return "response";
    }

    public List<TechRequest> getList() {
        return technicalBean.getRequests();
    }

    public int getTotalRequest(){
        return technicalBean.requestSize();
    }
    
    public String registerCustomer() {
        return null;
    }
}
