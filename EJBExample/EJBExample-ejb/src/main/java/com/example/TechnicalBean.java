/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example;

import com.example.model.TechRequest;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateful;


@Stateful
public class TechnicalBean implements TechnicalBeanLocal {

    private final List<TechRequest> requests = new ArrayList<TechRequest>();

    public void registerRequest(TechRequest req) {
        requests.add(req);
    }

    public int requestSize() {
        return requests.size();
    }

    public List<TechRequest> getRequests() {
        return requests;
    }
}
