/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example;

import com.example.model.TechRequest;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Local;


@Local
public interface TechnicalBeanLocal extends Serializable{
    void registerRequest(TechRequest req);
    int requestSize();
    List<TechRequest> getRequests();
}
